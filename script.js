function helper()
{
    document.getElementById("title").addEventListener("click", myFunction);
    var counter = 0;    //Counter for how many time u got hit

    function myFunction()
    {
        var canvas = document.querySelector('canvas');
        var ctx = canvas.getContext('2d');

        var width = canvas.width = window.innerWidth;
        var height = canvas.height = window.innerHeight;
        //mouse object holding position
        let mouse = 
        {
          x: 20,
          y: height,
        };

        addEventListener("mousemove", function(event)
        {
          mouse.x = event.clientX;
          mouse.y = event.clientY;
        });


        function random(min, max) 
        {
            var num = Math.floor(Math.random() * (max - min + 1)) + min;
            return num;
        }

        function Ball(x, y, velX, velY, color, size)
        {
            this.x = x;
            this.y = y;
            this.velX = velX;
            this.velY = velY;
            this.color = color;
            this.size = size;
        }
        //Constructor Function with Inheritance from Ball
//===================================================================
        function PlayerBall(x,y,velX,velY,color,size)
        {
            Ball.call(this, x, y, velX, velY, color, size);
        }
        PlayerBall.prototype = Object.create(Ball.prototype);
        PlayerBall.prototype.constructor = PlayerBall;
//===================================================================
        PlayerBall.prototype.winAlert = function()
        {
            setTimeout(winAlert, 5000);
            function winAlert()
            {
                if(counter == 0)
                {
                    alert("Congrats. You were hit " + counter + " times and survived!. Hit 'Refresh to play again!");
                }
                else
                {
                    alert("End of Game. You suffered " + counter + " points of damage and died! Hit 'Refresh' to play again!");
                }
            }
            console.log(ballPlayer.winAlert);
        };

        Ball.prototype.draw = function() 
        {
            ctx.beginPath();
            ctx.fillStyle = this.color;
            ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
            ctx.fill();
        }

        Ball.prototype.update = function() 
        {
            this.x += this.velX;
            this.y += this.velY;
        }
        Ball.prototype.collisionDetection = function()
        {
          for(let j = 0; j < ballArray.length; j++)
          {
            if(!(this === ballArray[j]))
            {
              let dx = ballArray[j].x - ballPlayer.x;
              let dy = ballArray[j].y - ballPlayer.y;

              let distance = Math.sqrt(Math.pow(dx,2) + Math.pow(dy,2));

              if(distance < ballArray[j].size + ballPlayer.size)
              {
                ballPlayer.color =  'rgb(' + random(0,255) + ',' + random(0,255) + ',' + random(0,255) +')';
                counter += 1;
              }

              //Change bombs color
              let distanceX = this.x - ballArray[j].x; 
              let distanceY = this.y - ballArray[j].y;

              let distanceTotal = Math.sqrt(Math.pow(distanceX,2) + Math.pow(distanceY,2));

              if(distanceTotal < this.size + ballArray[j].size)
              {
                ballArray[j].color = this.color = 'rgb(' + random(200,255) + ',' + random(0,150) + ',' + 0 +')';
              }
              //=================================================================================
            }

          }

        }

        //=====================================================================
        var ballArray = [];
        var ballPlayer;
        function init()
        {
          ballPlayer = new PlayerBall(undefined, undefined, 0, 0, "green", 40);
        }
        init();
        //Ball(x, y, velX, velY, color, size)
        while (ballArray.length < 50) 
        {
          var size = random(10,30);
          var ball = new Ball(random(0 + size,width - size),  random(size + size,height*.2),  random(-1,1), random(4,8), 'orangered', size);

          ballArray.push(ball);
        }

        function loop() 
        {
            var img = new Image();
            img.src = "https://wallpapercave.com/wp/Ymbi2k5.jpg"; 
            img.onload = ctx.drawImage(img, 0, 0, width, height);
            ctx.fillStyle = 'rgba(0, 0, 0, 0.25)';
            ctx.fillRect(0, 0, width, height);
          
            for (var i = 0; i < ballArray.length; i++) 
            {
              ballArray[i].draw();
              ballArray[i].collisionDetection();              
              ballArray[i].update();
            }
            ballPlayer.x = mouse.x;
            ballPlayer.y = height;

            ballPlayer.draw();

            requestAnimationFrame(loop);
        }
      loop();
      ballPlayer.winAlert();
    }
}

helper();



///=================================================================================================================
/*

function Speaker(givenName, surname, email)
{
    this.givenName = givenName;
    this.surname = surname;
    this.email = email;
    this.isActive = true;
}

Speaker.prototype.getBiography = function()
{
    console.log(this.surname);
    console.log("He was a good man. He liked the Godzilla movie and marshmellows");
}
Speaker.prototype.markInactive = function(date)
{
    this.isActive = false;
    this.inactiveDate = date;
}

function KeynoteSpeaker(givenName, surname, email, websites, keynoteTopics, breakouts)
{
    Speaker.call(this, givenName, surname, email);
}
KeynoteSpeaker.prototype = Object.create(Speaker.prototype);

function WorkshopSpeaker(givenName, surname, email, websites, keynoteTopics, breakouts)
{
    Speaker.call(this, givenName, surname, email);
}
WorkshopSpeaker.prototype = Object.create(Speaker.prototype);

var speakerInvoked = new Speaker("Bryan", "DZ", "bryfern710@gmail.com");
console.log(speakerInvoked);

var result2 = new KeynoteSpeaker("bry", "crocodile", "this@one");
console.log(result2);

var result3 = new WorkshopSpeaker("Mary", "little", "lambe@wolf");
console.log(result3);*/